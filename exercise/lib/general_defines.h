/** 
 * \file   hardware_configuration.h
 * \author Ludwig Kragl
 * \date 21 de Outubro de 2017, 15:31
 * \brief Defines generel values which make the code more easy to understand.
 */

#ifndef GENERAL_DEFINES_H
#define	GENERAL_DEFINES_H

#ifdef	__cplusplus
extern "C" {
#endif

#define OUTPUT 0
#define INPUT 1	
	
#define ON_ 1
#define OFF_ 0

#define true 1
#define false 0	
	
#ifdef	__cplusplus
}
#endif

#endif	/* HARDWARE_CONFIGURATION_H */

