/** 
 * \file:   hardware_configuration.h
 * \author: ludwig
 *
 * \date 22 de Outubro de 2017, 12:31
 */

#ifndef HARDWARE_CONFIGURATION_H
#define	HARDWARE_CONFIGURATION_H

#ifdef	__cplusplus
extern "C" {
#endif

#define SYSCLK 80000000L // System clock frequency, in Hz
#define PBUSCLK 40000000L // Peripheral bus clock

//One signal led, another led to identify next bit even if is has the same value and input pins
#define LED_TRIS TRISB
#define SIGNAL RB4
#define SIGNAL_LED_PORTbits PORTBbits
#define IDENTIFIER RB5
#define IDENTIFIER_LED_PORTbits PORTBbits
	
#define PARALLEL_INPUT_TRIS TRISE 
#define PARALLEL_INPUT_PORTbits PORTEbits
#define PARALLEL_INPUT0 RE0
#define PARALLEL_INPUT1 RE1
#define PARALLEL_INPUT2 RE2
#define PARALLEL_INPUT3 RE3
#define PARALLEL_INPUT4 RE4
#define PARALLEL_INPUT5 RE5
#define PARALLEL_INPUT6 RE6
#define PARALLEL_INPUT7 RE7
	
#ifdef	__cplusplus
}
#endif

#endif	/* HARDWARE_CONFIGURATION_H */

