/* 
 * \file   main.c
 * \author Ludwig Kragl
 * \date 20 de Outubro de 2017, 13:19
 * \brief Read on a 8 bit Port an output the values on a single LED.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h> //memcpy, memcmp
#define _SUPPRESS_PLIB_WARNING 1
#include <plib.h>
#include "hardware_configuration.h"
#include "general_defines.h"
#include "delay.h"

#define CONTINOUS_SERIAL_OUTPUT 1
#define ON_CHANGE_SERIAL_OUTPUT 2

#define EXERCISE ON_CHANGE_SERIAL_OUTPUT
#define WORK_AROUND_READ_PORT 1

int main(int argc, char** argv) {
	// Performance optimization (flash access time, enable instruct and data cache,... and PBClock setup
    SYSTEMConfigPerformance(SYSCLK);
    mOSCSetPBDIV( OSC_PB_DIV_2 ); // This is necessary since SYSTEMConfigPerformance defaults FPBDIV to DIV_1
	
	//PIN DIRECTION
	PARALLEL_INPUT_TRIS=0xFF; //input
	LED_TRIS = OUTPUT;	
	
	int i;
#if WORK_AROUND_READ_PORT==0
	long int measurement;
	long int pre_measurement;
	
	while(1){
		measurement=LATE; //Read the whole port /** \todo change mask to 8 pins*/
		measurement= measurement&0b1111;
#if EXERCISE==ON_CHANGE_SERIAL_OUTPUT
		if(measurement!=pre_measurement){
#endif
			pre_measurement = measurement;
			for(i=0; i<4; i++){
				PORTBbits.RB4 = (measurement&(1<<i))>0; //If the i-te bit of measurement is bigger then 1 set the led
				delay(1500); //signal_time
				PORTBbits.RB5 = 1; //Set LED of to recognice different signal values
				delay(50);
				PORTBbits.RB5 = 0;
			}
			PORTBbits.RB5=1;
			delay(400);
			PORTBbits.RB5=0;
#if EXERCISE==ON_CHANGE_SERIAL_OUTPUT
		}
#endif //EXERCISE==ON_CHANGE_SERIAL_OUTPUT		
		pre_measurement = measurement;
		delay_ms(100);
	}
#else //WORK_AROUND_READ_PORT
	int measurement_array[8];
	int pre_measurement_array[8]={0};
	
	while(1){
		//READ IN ALL PINS
		measurement_array[0]=PARALLEL_INPUT_PORTbits.PARALLEL_INPUT0;
		measurement_array[1]=PARALLEL_INPUT_PORTbits.PARALLEL_INPUT1;
		measurement_array[2]=PARALLEL_INPUT_PORTbits.PARALLEL_INPUT2;
		measurement_array[3]=PARALLEL_INPUT_PORTbits.PARALLEL_INPUT3;
		measurement_array[4]=PARALLEL_INPUT_PORTbits.PARALLEL_INPUT4;
		measurement_array[5]=PARALLEL_INPUT_PORTbits.PARALLEL_INPUT5;
		measurement_array[6]=PARALLEL_INPUT_PORTbits.PARALLEL_INPUT6;
		measurement_array[7]=PARALLEL_INPUT_PORTbits.PARALLEL_INPUT7;
#if EXERCISE==ON_CHANGE_SERIAL_OUTPUT
		if(memcmp(measurement_array, pre_measurement_array, sizeof(measurement_array))){
#endif
			for(i=0; i<8; i++){
				SIGNAL_LED_PORTbits.SIGNAL = measurement_array[i]; //If the i-te bit of measurement is bigger then 1 set the led
				delay_ms(1500); //signal_time
				IDENTIFIER_LED_PORTbits.IDENTIFIER = ON_; //Set LED of to recognice different signal values
				delay_ms(50);
				IDENTIFIER_LED_PORTbits.IDENTIFIER = OFF_;
			}
			//SIGNAL END OF SENDING:
			SIGNAL_LED_PORTbits.SIGNAL=OFF_;
			IDENTIFIER_LED_PORTbits.IDENTIFIER=ON_;
			delay_ms(400);
			IDENTIFIER_LED_PORTbits.IDENTIFIER=OFF_;
#if EXERCISE==ON_CHANGE_SERIAL_OUTPUT
		}
#endif
		memcpy(pre_measurement_array, measurement_array, sizeof(measurement_array));
		delay_ms(50);
	}
#endif //WORK_AROUND_READ_PORT
	return (EXIT_SUCCESS);
}