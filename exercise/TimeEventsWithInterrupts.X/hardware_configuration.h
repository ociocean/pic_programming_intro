/** 
 * \file:   hardware_configuration.h
 * \author: ludwig
 *
 * \date 22 de Outubro de 2017, 12:31
 */

#ifndef HARDWARE_CONFIGURATION_H
#define	HARDWARE_CONFIGURATION_H

#ifdef	__cplusplus
extern "C" {
#endif

#define SYSCLK 80000000L // System clock frequency, in Hz
#define PBUSCLK 40000000L // Peripheral bus clock

//Configuration for 2 pwm leds
#define LED_PORT PORTD
#define LED_TRIS TRISD
#define LED_LAT LATD
#define LED_PORTbits PORTDbits
#define LED_TRISbits TRISDbits
	
#define LEDA_TRISbit TRISD0
#define LEDA RD0
#define LEDA_INTERRUPT_FLAG T3IF
#define LEDB_TRISbit TRISD1
#define LEDB RD1
	
#ifdef	__cplusplus
}
#endif

#endif	/* HARDWARE_CONFIGURATION_H */

