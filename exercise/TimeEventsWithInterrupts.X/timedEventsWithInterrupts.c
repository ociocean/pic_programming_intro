/* 
 * \file	timedEventsWithInterrupts.c
 * \author	Ludwig Kragl
 * \date 15 de Outubro de 2017, 21:25
 * \brief Main skript with implementations of exercise Timed Events with Interrupts
 * 
 * Change the define of EXERCISE to switch between the different exercise numbers
 */

#include <stdio.h>
#include <stdlib.h>
#include <p32xxxx.h>
#define _SUPPRESS_PLIB_WARNING 1
#include <plib.h>
#include "hardware_configuration.h"
#include "general_defines.h"
#include "timer.h"

#define REPEAT_SIMPLEIO_1 1
#define TWO_SQUARE_WAVES 2
#define FREE_TIME_AND_PERIOD_SETTING 3

#define EXERCISE REPEAT_SIMPLEIO_1

#if EXERCISE==REPEAT_SIMPLEIO_1
#define WORK_AOROUND_NOT_WORKING_32BIT_INTERRUPT 1
/** \attention Does not work anymore. Look in git repository for working version*/
void init_timer2();
void init_timer2_interrupt();

int n=0;

int main(int argc, char** argv){
	// Performance optimization (flash access time, enable instruct and data cache,... and PBClock setup
    SYSTEMConfigPerformance(SYSCLK);
    mOSCSetPBDIV(OSC_PB_DIV_2); // This is necessary since SYSTEMConfigPerformance defaults FPBDIV to DIV_1
	// Setup to use the external interrupt controller
    INTEnableSystemMultiVectoredInt();
	
	// Set port D as output
    LED_TRIS = OUTPUT;	//output
	LED_PORT = 0x55;
	
    //SETPUP TIMER2	
    // To generate a frequency of 2Hz a 32bit timer is needed. Timer 2 is the first possible 32bit Timer:
    init_timer2(); //32bit timer!
	init_timer2_interrupt();
	T2CONbits.ON = 1; //Enable timer			
    
	//SWITCH THE LED EVERYTIME A INTERRUPT FLAG APPEARS
    while(true);
}

/** \brief Setup the timer to generate an interrupt flag after 500ms (2Hz) */
void init_timer2(){
#if WORK_AOROUND_NOT_WORKING_32BIT_INTERRUPT==0
	T2CON=0; //Stop timer2
    T3CON=0; //Stop timer3
    T2CONbits.TCKPS=TPS_1; //Set Prescaler
    TMR2 = 0; //Clear Timer register
	TMR3 = 0; //Clear Timer register
    T2CONbits.T32 = 1; //Enable 32 bit timer -> timer 3 is now used for this timer
    PR2=PBUSCLK*0.5/1; //Set periode 0.5ms; prescaler 1
#else
	T2CON=0; //Stop timer2
    T2CONbits.TCKPS=TPS_256; //Set Prescaler
    TMR2 = 0; //Clear Timer register
    T2CONbits.T32 = 0; //Enable 32 bit timer -> timer 3 is now used for this timer
    PR2=PBUSCLK*0.25/1; //Set periode 0.5ms/2; prescaler 1
#endif
}

void init_timer2_interrupt(){
#if WORK_AOROUND_NOT_WORKING_32BIT_INTERRUPT==0
	void __attribute__( (interrupt(IPL2AUTO), vector(_TIMER_3_VECTOR))) tmr3_isr(void);
	IFS0bits.T3IF = 0;// Reset timer T2 interrupt flag
	IPC3bits.T3IP = 2;// Interrupt priority (must be in range [1..6])
	IEC0bits.T3IE = 1;// Enable timer T3 interrupts
#else
	void __attribute__( (interrupt(IPL2AUTO), vector(_TIMER_2_VECTOR))) tmr2_isr(void);
	IFS0bits.T2IF = 0;// Reset timer T2 interrupt flag
	IPC2bits.T2IP = 2;// Interrupt priority (must be in range [1..6])
	IEC0bits.T2IE = 1;// Enable timer T3 interrupts
#endif
}

void tmr2_isr(void) {
	LED_PORTbits.RD7=1; //debug led
	n++;
	if(n%2==0){
		if(LED_LAT==0x55){
			LED_PORT=0xaa;
		}
		else{
			LED_PORT=0x55;
		}
	}
    IFS0bits.T3IF = 0;
}

void tmr3_isr(void) {
	LED_PORTbits.RD7=1; //debug led
    if(LED_LAT==0x55){
		LED_PORT=0xaa;
	}
	else{
		LED_PORT=0x55;
	}
    IFS0bits.T3IF = 0;
}
#endif

#if EXERCISE==TWO_SQUARE_WAVES

#define PRESCALER 256
#define PRESCALER_BITS TPS_256
#define BASE_FREQUENCY 3 //With the current implementation 3 is the smallest frequency
#define MULTIPLE_OF_BASE_FREQUENCY 3

void init_timer2();
void init_timer2_interrupt();

int k=0;

int main(int argc, char** argv) {	
	// Performance optimization (flash access time, enable instruct and data cache,... and PBClock setup
    SYSTEMConfigPerformance(SYSCLK);
    mOSCSetPBDIV( OSC_PB_DIV_2 ); // This is necessary since SYSTEMConfigPerformance defaults FPBDIV to DIV_1
	// Setup to use the external interrupt controller
    INTEnableSystemMultiVectoredInt();
	
	// Set port D as output
    LED_TRIS = OUTPUT;	//output
	LED_PORT = OFF_;
	
	//SET TIMER 2 DEPENDENT OF HIGH FREQUENCIES
	init_timer2();
	init_timer2_interrupt();

    //START TIMER2 AND ENTER MAIN LOOP
    T2CONbits.TON=1; // Start the timer
    while(true){
    }
    return (EXIT_SUCCESS);
}

void tmr2_isr(void) {
    LED_PORTbits.LEDB = !LED_PORTbits.LEDB;
	k++;
	if(k%MULTIPLE_OF_BASE_FREQUENCY==0){
		LED_PORTbits.LEDA = !LED_PORTbits.LEDA;
	}
    IFS0bits.T2IF = 0;
}

/** \brief Setup the timer to generate an interrupt flag according to the desired HIGHER_FRQUENCY*/
void init_timer2(){
	T2CON=0; //Stop timer2
    T2CONbits.TCKPS=PRESCALER_BITS; //Set Prescaler
    TMR2 = 0; //Clear Timer register
    T2CONbits.T32 = 0; //Disbale 32 bit timer
    PR2=PBUSCLK/(BASE_FREQUENCY*MULTIPLE_OF_BASE_FREQUENCY)/PRESCALER-1; //Set periode 0.5ms; prescaler 1
}

void init_timer2_interrupt(){
	void __attribute__( (interrupt(IPL2AUTO), vector(_TIMER_2_VECTOR))) tmr2_isr(void);
	IFS0bits.T2IF = 0;// Reset timer T2 interrupt flag
	IPC2bits.T2IP = 2;// Interrupt priority (must be in range [1..6])
	IEC0bits.T2IE = 1;// Enable timer T3 interrupts
}

#endif

#if EXERCISE==FREE_TIME_AND_PERIOD_SETTING
#define PRESCALER 256
#define PRESCALER_BITS TPS_256
#define T_PERIOD1 0.4 //[s] Maximum is 0,419424 s because a 16bit timer is choosen and max prescaler
#define T_ON1 0.01	//T_ON1<T_PERIOD1
#define T_PERIOD2 0.3 //[s] Maximum is 0,419424 s because a 16bit timer is choosenand max prescaler
#define T_ON2 0.05 // T_ON2<T_PERIOD2

#define T_OFF1 T_PERIOD1-T_ON1
#define T_OFF2 T_PERIOD2-T_ON2

void init_timer2();
void init_timer3();

int state1;
int state2;

int main(int argc, char** argv) {
	// Performance optimization (flash access time, enable instruct and data cache,... and PBClock setup
	SYSTEMConfigPerformance(SYSCLK);
	mOSCSetPBDIV(OSC_PB_DIV_2); // This is necessary since SYSTEMConfigPerformance defaults FPBDIV to DIV_1
	// Setup to use the interrupt controller
    INTEnableSystemMultiVectoredInt();

	//HARDWARE CONFIGURATION:
	LED_TRIS= OUTPUT;
	LED_PORT=OFF_;
	
	//CONFIG TIMERS INCLUDING INTERRUPTS
	init_timer2();
	init_timer3();
	
	LED_PORTbits.LEDA=ON_;
	LED_PORTbits.LEDB=ON_;
	
	T2CONbits.TON = 1;
	T3CONbits.TON = 1;
	state1 = ON_;
	state2 = ON_;
	
	while(1);
	return 0;
}

void tmr2_isr(void) {
	switch(state1){
		case ON_:
			LED_PORTbits.LEDA=0;
			PR2=PBUSCLK*T_OFF1/PRESCALER;
			state1=OFF_;
			break;
		case OFF_:
			LED_PORTbits.LEDA=1;
			PR2=PBUSCLK*T_ON1/PRESCALER;
			state1=ON_;
			break;
	}
	IFS0bits.T2IF = 0;
}

void tmr3_isr(void) {
	switch(state2){
		case ON_:
			LED_PORTbits.LEDB=0;
			PR3=PBUSCLK*T_OFF2/PRESCALER;
			state2=OFF_;
			break;
		case OFF_:
			LED_PORTbits.LEDB=1;
			PR3=PBUSCLK*T_ON2/PRESCALER;
			state2=ON_;
			break;
	}
	IFS0bits.T3IF = 0;
}

void init_timer2(){
	void __attribute__( (interrupt(IPL5AUTO), vector(_TIMER_2_VECTOR))) tmr2_isr(void);
	T2CONbits.ON = 0; // Stop timer
    IFS0bits.T2IF=0; // Reset interrupt flag
    IPC2bits.T2IP=5; //set interrupt priority (1..7) *** Make sure it matches iplx in isr declaration above ***
    IEC0bits.T2IE = 1; // Enable T2 interrupts
	T2CONbits.TCKPS = PRESCALER_BITS; //256 Set pre-scaler
	T2CONbits.T32 = 0; // 16 bit timer operation
	PR2 = T_ON1*PBUSCLK/PRESCALER; // Compute PR value
}

void init_timer3(){
	void __attribute__( (interrupt(IPL4AUTO), vector(_TIMER_3_VECTOR))) tmr3_isr(void);
	T3CONbits.ON = 0; // Stop timer
    IFS0bits.T3IF=0; // Reset interrupt flag
    IPC3bits.T3IP=4; //set interrupt priority (1..7) *** Make sure it matches iplx in isr declaration above ***
    T3CONbits.TCKPS = PRESCALER_BITS; //256; Set pre-scaler
	IEC0bits.T3IE = 1; // Enable T2 interrupts
	PR3 = T_ON2*PBUSCLK/PRESCALER; //3Hz is slowest whole-numbered frequency with an 16 bit timer
}
#endif