/* 
 * \file   concurrency.c
 * \author Ludwig Kragl
 * \date on 21 de Outubro de 2017, 15:39
 */

#include <stdio.h>
#include <stdlib.h>
#define _SUPPRESS_PLIB_WARNING 1
#include <plib.h>
#include <proc/p32mx795f512h.h>
#include "general_defines.h"
#include "hardware_configuration.h"
#include "timer.h"

#define ONE_STOPEABLE_BLINKER 1
#define LOG_RISING_FALLING_EDGES 2

#define EXERCISE ONE_STOPEABLE_BLINKER

#if EXERCISE==ONE_STOPEABLE_BLINKER
#define PRESCALER 1
#define PRESCALER_BITS TPS_1

void init_timer2();

int main(int argc, char** argv) {
	// Performance optimization (flash access time, enable instruct and data cache,... and PBClock setup
	SYSTEMConfigPerformance(SYSCLK);
	mOSCSetPBDIV(OSC_PB_DIV_2); // This is necessary since SYSTEMConfigPerformance defaults FPBDIV to DIV_1

	//HARDWARE CONFIGURATION:
	SWITCH_TRISbits.SWITCHA_TRISbit = INPUT;
	LED_TRISbits.LEDA_TRIS_bit = OUTPUT;
	LED_TRISbits.LEDB_TRIS_bit = OUTPUT;
	
	//SWITCH LEDs OFF
	LED_PORTbits.LEDA = OFF_;
	LED_PORTbits.LEDB = OFF_;
	
    //SETPUP TIMER2
    // Timer 2 is the first possible 32bit Timer:
    init_timer2();
	T2CONbits.ON = 1; //Enable timer
	
	while(true){
        if(IFS0bits.T3IF==1){
			IFS0bits.T3IF=0;
			LED_PORTbits.LEDA = !LED_PORTbits.LEDA;
			if(SWITCH_LATbits.SWITCHA){
				LED_PORTbits.LEDB = !LED_PORTbits.LEDB;
			}
		}
    }

	return (EXIT_SUCCESS);
}

void init_timer2(){
	T2CON=0; //Stop timer2
    T3CON=0; //Stop timer3
    T2CONbits.TCKPS=PRESCALER_BITS; //Set Prescaler
    TMR2 = 0; //Clear Timer register
	TMR3 = 0; //Clear Timer register
    T2CONbits.T32 = 1; //Enable 32 bit timer -> timer 3 is no used for this timer
    PR2=PBUSCLK/0.5/PRESCALER; //Set periode 0.5ms; prescaler 1
}
#endif //EXERCISE==1
#if EXERCISE==2

#define TOGGLE_LTOH 1

int main(int argc, char** argv) {
	/** \brief
	 * Strategy: If an rising/falling edge is detected an external ISR is called.
	 * In this the led is set on and an timer is started. If the timer overflows
	 * a timer interrupt is called which sets the led of and disables the timer.	 * 
     */
	// Performance optimization (flash access time, enable instruct and data cache,... and PBClock setup
	SYSTEMConfigPerformance(SYSCLK);
	mOSCSetPBDIV(OSC_PB_DIV_2); // This is necessary since SYSTEMConfigPerformance defaults FPBDIV to DIV_1
	// Setup to use the interrupt controller
    INTEnableSystemMultiVectoredInt();

	//HARDWARE CONFIGURATION:
	SWITCH_TRISbits.SWITCHA_TRISbit = INPUT;
	SWITCH_TRISbits.SWITCHB_TRISbit = INPUT;
	LED_TRISbits.LEDA_TRISbit = OUTPUT;
	TRISDbits.LEDB_TRISbit = OUTPUT;
	
	//SWITCH LEDs OFF
	LED_PORTbits.LEDA = OFF_;
	LED_PORTbits.LEDB = OFF_;
	
	//SETUP EXTERNAL INTERRUPTS:
	void __attribute__((interrupt(IPL1AUTO), vector(_EXTERNAL_1_VECTOR))) int1_isr(void); 
	INTCONbits.INT1EP = TOGGLE_LTOH; // Generat interrupts on {rising edge-1 falling edge - 0}
    IFS0bits.INT1IF = 0; // Reset int flag
    IPC1bits.INT1IP = 1; // Set interrupt priority (1..7)
    IEC0bits.INT1IE = 1; // Enable Int1 interrupts
	
	void __attribute__((interrupt(IPL2AUTO), vector(_EXTERNAL_2_VECTOR))) int2_isr(void);
	INTCONbits.INT2EP = TOGGLE_LTOH; // Generat interrupts on {rising edge-1 falling edge - 0}
    IFS0bits.INT2IF = 0; // Reset int flag
    IPC2bits.INT2IP = 2; // Set interrupt priority (1..7)
    IEC0bits.INT2IE = 1; // Enable Int1 interrupts
	
	//CONFIG TIMERS
	void __attribute__( (interrupt(IPL3AUTO), vector(_TIMER_2_VECTOR))) tmr2_isr(void);
	T2CONbits.ON = 0; // Stop timer
    IFS0bits.T2IF=0; // Reset interrupt flag
    IPC2bits.T2IP=3; //set interrupt priority (1..7) *** Make sure it matches iplx in isr declaration above ***
    IEC0bits.T2IE = 1; // Enable T2 interrupts
	T2CONbits.TCKPS = 0b111; //256 Set pre-scaler
	T2CONbits.T32 = 0; // 16 bit timer operation
	PR2 = PBUSCLK/256/3-1; //3Hz is slowest whole-numbered frequency with an 16 bit timer
	
	void __attribute__( (interrupt(IPL4AUTO), vector(_TIMER_3_VECTOR))) tmr3_isr(void);
	T3CONbits.ON = 0; // Stop timer
    IFS0bits.T3IF=0; // Reset interrupt flag
    IPC3bits.T3IP=4; //set interrupt priority (1..7) *** Make sure it matches iplx in isr declaration above ***
    IEC0bits.T3IE = 1; // Enable T2 interrupts
	T3CONbits.TCKPS = 0b111; //256; Set pre-scaler
	PR3 = PBUSCLK/256/3-1; //3Hz is slowest whole-numbered frequency with an 16 bit timer
	
	while(1){
		//The remaining work is done in the ISRs
	}
}

void int1_isr(void) {
    LED_PORTbits.LEDA = ON_;
	TMR2=0;
    T2CONbits.TON=1; // Start the timer
	//IEC0bits.INT1IE = 0; //Disable interrupt1
    IFS0bits.INT1IF = 0; // Reset interrupt flag 
}

void int2_isr(void) {
    LED_PORTbits.LEDB = ON_;
	TMR3=0;
    T3CONbits.TON=1; // Start the timer
	//IEC0bits.INT2IE = 0; //Disable interrupt2
    IFS0bits.INT2IF = 0; // Reset interrupt flag in tmr2_isr()
}

void tmr2_isr(void) {
	T2CONbits.TON=0; //Stop the timer
    LED_PORTbits.LEDA = OFF_;	//Switch LED off
	//IEC0bits.INT1IE = 1; //Enable interrupt1
    IFS0bits.T2IF = 0;	//Clear interrupt flag timer2
}

void tmr3_isr(void) {
	T3CONbits.TON=0; //Stop the timer
    LED_PORTbits.LEDB = OFF_;
	//IEC0bits.INT2IE = 1; //Enable interrupt2
    IFS0bits.T3IF = 0; //Clear interrupt flag timer3
}
#endif //EXERCISE==2
