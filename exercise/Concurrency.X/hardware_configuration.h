/** 
 * \file:   hardware_configuration.h
 * \author: ludwig
 *
 * \date 22 de Outubro de 2017, 12:31
 */

#ifndef HARDWARE_CONFIGURATION_H
#define	HARDWARE_CONFIGURATION_H

#ifdef	__cplusplus
extern "C" {
#endif

#define SYSCLK 80000000L // System clock frequency, in Hz
#define PBUSCLK 40000000L // Peripheral bus clock

//two switches and two leds for independent edge detection
#define SWITCH_TRISbits TRISDbits
#define SWITCHA_TRISbit TRISD8
#define SWITCHB_TRISbit TRISD9
#define SWITCH_LATbits LATDbits
#define SWITCHA 
#define LED_TRISbits TRISDbits
#define LEDA_TRISbit TRISD0
#define LEDB_TRISbit TRISD1
	
#define LED_PORTbits PORTDbits
#define LEDA RD0
#define LEDB RD1
 	
#ifdef	__cplusplus
}
#endif

#endif	/* HARDWARE_CONFIGURATION_H */

