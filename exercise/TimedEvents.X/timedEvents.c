/* 
 * \file	timedEvents.c
 * \author	Ludwig Kragl
 * \date 15 de Outubro de 2017, 21:25
 * \brief Main skript with implementations of exercise Timed Events without Interrupts.
 * 
 * Change the define of EXERCISE to switch between the different exercise numbers
 */

#include <stdio.h>
#include <stdlib.h>
#include <p32xxxx.h>
#define _SUPPRESS_PLIB_WARNING 1
#include <plib.h>
#include <math.h>
#include "hardware_configuration.h"
#include "general_defines.h"
#include "timer.h"

#define REPEAT_SIMPLEIO_1 1
#define TWO_SQUARE_WAVES 2
#define FREE_TIME_AND_PERIOD_SETTING 3

#define EXERCISE FREE_TIME_AND_PERIOD_SETTING

#if EXERCISE==REPEAT_SIMPLEIO_1
void init_timer2();

int main(int argc, char** argv){
	// Performance optimization (flash access time, enable instruct and data cache,... and PBClock setup
    SYSTEMConfigPerformance(SYSCLK);
    mOSCSetPBDIV(OSC_PB_DIV_2); // This is necessary since SYSTEMConfigPerformance defaults FPBDIV to DIV_1
	
	// Set port D as output
    LED_TRIS = OUTPUT;	//output
	LED_PORT = 0x55;
	
    //SETPUP TIMER2	
    // To generate a frequency of 2Hz a 32bit timer is needed. Timer 2 is the first possible 32bit Timer:
    init_timer2();
	T2CONbits.ON = 1; //Enable timer			
    
	//SWITCH THE LED EVERYTIME A INTERRUPT FLAG APPEARS
    while(true){
        if(IFS0bits.LEDA_INTERRUPT_FLAG==1){
			IFS0bits.LEDA_INTERRUPT_FLAG=0;
			if(LED_LAT==0x55){
				LED_PORT=0xaa;
			}
			else{
				LED_PORT=0x55;
			}
		}
    }
}

/** \brief Setup the timer to generate an interrupt flag after 500ms (2Hz) */
void init_timer2(){
	T2CON=0; //Stop timer2
    T3CON=0; //Stop timer3
    T2CONbits.TCKPS=TPS_1; //Set Prescaler
    TMR2 = 0; //Clear Timer register
	TMR3 = 0; //Clear Timer register
    T2CONbits.T32 = 1; //Enable 32 bit timer -> timer 3 is no used for this timer
    PR2=PBUSCLK*0.5/1; //Set periode 0.5ms; prescaler 1
}
#endif

#if EXERCISE==TWO_SQUARE_WAVES

#define BASE_FREQUENCY 2
#define MULTIPLE_OF_BASE_FREQUENCY 3
#define PRESCALER 1
#define PRESCALER_BITS TPS_1

void init_timer2();

int main(int argc, char** argv) {	
	// Performance optimization (flash access time, enable instruct and data cache,... and PBClock setup
    SYSTEMConfigPerformance(SYSCLK);
    mOSCSetPBDIV( OSC_PB_DIV_2 ); // This is necessary since SYSTEMConfigPerformance defaults FPBDIV to DIV_1
	
	// Set port D as output
    LED_TRIS = OUTPUT;	//output
	LED_PORT = OFF_;
	
	//SET TIMER 2 DEPENDENT OF HIGH FREQUENCIES
	init_timer2();

    //START TIMER2 AND ENTER MAIN LOOP
	int k=0;
    T2CONbits.TON=1; // Start the timer
    while(true){
		if(IFS0bits.LEDA_INTERRUPT_FLAG==1){
			IFS0bits.LEDA_INTERRUPT_FLAG=0;
			LED_PORTbits.LEDB = !LED_PORTbits.LEDB;
			k++;
			if(k%MULTIPLE_OF_BASE_FREQUENCY==0){
				LED_PORTbits.LEDA = !LED_PORTbits.LEDA;
			}
		}
    }
    return (EXIT_SUCCESS);
}

/** \brief Setup the timer to generate an interrupt flag according to the desired HIGHER_FRQUENCY*/
void init_timer2(){
	T2CON=0; //Stop timer2
    T3CON=0; //Stop timer3
    T2CONbits.TCKPS=PRESCALER_BITS; //Set Prescaler
    TMR2 = 0; //Clear Timer register
	TMR3 = 0; //Clear Timer register
    T2CONbits.T32 = 1; //Enable 32 bit timer -> timer 3 is no used for this timer
    PR2=PBUSCLK/(BASE_FREQUENCY*MULTIPLE_OF_BASE_FREQUENCY)/PRESCALER; //Set periode 0.5ms; prescaler 1
}
#endif

#if EXERCISE==FREE_TIME_AND_PERIOD_SETTING
#define PRESCALER 256
#define T_PERIOD1 0.4 //[s] Maximum is 0,419424 s because a 16bit timer is choosen and max prescaler
#define T_ON1 0.04	//T_ON1<T_PERIOD1
#define T_PERIOD2 0.33 //[s] Maximum is 0,419424 s because a 16bit timer is choosenand max prescaler
#define T_ON2 0.126 // T_ON2<T_PERIOD2

void init_timer2();
void init_timer3();
void init_ocm1();
void init_ocm2();

int main(int argc, char** argv) {
	// Performance optimization (flash access time, enable instruct and data cache,... and PBClock setup
    SYSTEMConfigPerformance(SYSCLK);
    mOSCSetPBDIV( OSC_PB_DIV_2 ); // This is necessary since SYSTEMConfigPerformance defaults FPBDIV to DIV_1

    // Set timers
    init_timer2();
	init_timer3();
	
    // Set output compare modules:
    init_ocm1();
	init_ocm2();

    // Start PWM generation
    T2CONbits.TON=1; // Start the timer
	T3CONbits.TON=1; // Start the timer

    // Main loop
    while(1); // Do nothing!
    return (EXIT_SUCCESS);
}

void init_timer2(){
	T2CONbits.ON = 0; // Stop timer
    IFS0bits.T2IF=0; // Reset interrupt flag    
    T2CONbits.TCKPS = TPS_256; //Select pre-scaler
    T2CONbits.T32 = 0; // 16 bit timer operation
    PR2=T_PERIOD1*(float)PBUSCLK/PRESCALER; // Compute PR value
    TMR2=0;
}

void init_timer3(){
	T3CONbits.ON = 0; // Stop timer
    IFS0bits.T3IF=0; // Reset interrupt flag    
    T3CONbits.TCKPS = TPS_256; //Select pre-scaler
    PR2=T_PERIOD1*(float)PBUSCLK/PRESCALER; // Compute PR value
    TMR3=0;
}

void init_ocm1(){
	OC1CONbits.OCM = 6; // OCM = 0b110 : OC1 in PWM mode,
    OC1CONbits.OCTSEL=0; // Timer 2 is clock source of OCM
    OC1RS=T_ON1*(float)PBUSCLK/PRESCALER; // Compute OC1xRS value
    OC1CONbits.ON=1;     // Enable OC1
}

void init_ocm2(){
	OC2CONbits.OCM = 6; // OCM = 0b110 : OC1 in PWM mode,
    OC2CONbits.OCTSEL=1; // Timer 3 is clock source of OCM
    OC2RS=T_ON2*(float)PBUSCLK/PRESCALER; // Compute OC1xRS value
    OC2CONbits.ON=1;     // Enable OC1
}
#endif