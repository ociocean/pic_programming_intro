/** 
 * \file:   timer.h
 * \author: ludwig
 *
 * \date 22 de Outubro de 2017, 14:58
 */

#ifndef TIMER_H
#define	TIMER_H

#ifdef	__cplusplus
extern "C" {
#endif

//BIT DEFINITIONS FOR PRESCALER SETTINGS
#define TPS_256 7 // TCKPS code for xx pre-scaler
#define TPS_64  6
#define TPS_32  5
#define TPS_16  4
#define TPS_8   3
#define TPS_4   2
#define TPS_2   1
#define TPS_1   0


#ifdef	__cplusplus
}
#endif

#endif	/* TIMER_H */

