/** 
 * \file:   delay.h
 * \author: ludwig
 *
 * \date 22 de Outubro de 2017, 12:21
 */

#ifndef DELAY_H
#define	DELAY_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "hardware_configuration.h"
	
#define DEVIDER SYSCLK/10000

void delay_ms(int);	

#ifdef	__cplusplus
}
#endif

#endif	/* DELAY_H */

