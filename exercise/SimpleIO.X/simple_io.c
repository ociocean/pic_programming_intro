/** 
 * \file simple_io.c
 * \author Ludwig Kragl
 * \date 15 de Outubro de 2017, 17:04
 * \brief Main skript with implementations of exercise SimpleIO
 * 
 * Change the define of EXERCISE to switch between the different exercise numbers
 */
#include <stdio.h>
#include <stdlib.h>
#define _SUPPRESS_PLIB_WARNING 1
#include <plib.h>
#include "hardware_configuration.h"
#include "general_defines.h"
#include "delay.h"

#define EXERCISE1 1
#define EXERCISE2 2
#define EXERCISE3 3

//CHOOSE THE EXERCISE TO COMPILE HERE
#define EXERCISE EXERCISE3

#if EXERCISE==EXERCISE1
int main(int argc, char** argv) {
    // Performance optimization (flash access time, enable instruct and data cache,... and PBClock setup
    SYSTEMConfigPerformance(SYSCLK);
    mOSCSetPBDIV(OSC_PB_DIV_2); // This is necessary since SYSTEMConfigPerformance defaults FPBDIV to DIV_1
    int i, j, machine_state=0;
    
    //DEFINE OUTPUT PORTS:
    LED_TRIS = 0x00;
    
    //SET INITIAL OUTPUT VALUE
    LED_PORT = 0x55;
    
    //ENTER WHILE LOOP
    while(true){
        switch(machine_state){
            case 0:
                LED_PORT=0x55;
                machine_state=1;
                break;
            case 1:
                LED_PORT=0xAA;
                machine_state=0;
                break;
        }
        //WAIT FOR HALF A SECOND WITHOUT TIMER:
        delay_ms(500);
    }
    return (EXIT_SUCCESS);
}
#endif

#if EXERCISE==EXERCISE2
int main(int argc, char** argv) {
    // Performance optimization (flash access time, enable instruct and data cache,... and PBClock setup
    SYSTEMConfigPerformance(SYSCLK);
    mOSCSetPBDIV(OSC_PB_DIV_2); // This is necessary since SYSTEMConfigPerformance defaults FPBDIV to DIV_1
    int i, j, machine_state=0;
    
    //DEFINE IN AND OUTPUT PORTS:
    LED_TRIS = 0x00;
    SWITCH_TRISbits.SWITCH_TRISbit=INPUT; //intput
    
    //SET INITIAL OUTPUT VALUE
    LED_PORT = 0x55;
    
    //ENTER WHILE LOOP
    while(true){
        if(SWITCH_PORTbits.SWITCH){
            switch(machine_state){
                case 0:
                    LED_PORT=0x55;
                    machine_state=1;
                    break;
                case 1:
                    LED_PORT=0xAA;
                    machine_state=0;
                    break;
            }
        }
        //WAIT FOR HALF A SECOND WITHOUT TIMER:
        delay_ms(500);
    }
    return (EXIT_SUCCESS);
}
#endif

#if EXERCISE==EXERCISE3
int main(int argc, char** argv) {
    // Performance optimization (flash access time, enable instruct and data cache,... and PBClock setup
    SYSTEMConfigPerformance(SYSCLK);
    mOSCSetPBDIV(OSC_PB_DIV_2); // This is necessary since SYSTEMConfigPerformance defaults FPBDIV to DIV_1
    int i, j, k, machine_state=0;
    
    
    //DEFINE IN AND OUTPUT PORTS:
    LED_TRIS = 0x00;
    SWITCH_TRISbits.SWITCH_TRISbit=INPUT; //input
    
    //SET INITIAL OUTPUT VALUE
    LED_PORT = 0x55;
    
    int prev_pinvalue = 0;
    //ENTER WHILE LOOP
    while(true){
        //RISING EDGE:
        if((SWITCH_PORTbits.SWITCH) & (prev_pinvalue!=0)){
            LED_PORT=0xAA;
            machine_state=0;
            
            for(k=0; k<5; k++){
				//WAIT FOR HALF A SECOND WITHOUT TIMER:
                delay_ms(500);
				
                //SWITCH LEDS
                switch(machine_state){
                    case 0:
                        LED_PORT=0x55;
                        machine_state=1;
                        break;
                    case 1:
                        LED_PORT=0xAA;
                        machine_state=0;
                        break;
                }
            }
        }
        
        //UPDATE INPUT
        prev_pinvalue=SWITCH_PORTbits.SWITCH;
        
        //LITTLE DELAY TO DEBOUNCE --> BUTTON MUST BE HOLD LONGER TO BE DETECTED
        delay_ms(200); 
    }
    return (EXIT_SUCCESS);
}
#endif