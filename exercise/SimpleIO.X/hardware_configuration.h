/** 
 * \file:   hardware_configuration.h
 * \author: ludwig
 *
 * \date 22 de Outubro de 2017, 12:31
 */

#ifndef HARDWARE_CONFIGURATION_H
#define	HARDWARE_CONFIGURATION_H

#ifdef	__cplusplus
extern "C" {
#endif

#define SYSCLK 80000000L // System clock frequency, in Hz
#define PBUSCLK 40000000L // Peripheral bus clock

//8 LEDs are connected to the first pins of Port E
#define LED_PORT PORTE
#define LED_TRIS TRISE

//one switch is connected to PortD.5 with pulldown resistor	
#define SWITCH_TRISbits TRISDbits
#define SWITCH_TRISbit TRISD5
#define SWITCH_PORTbits PORTDbits
#define SWITCH RD5
	
#ifdef	__cplusplus
}
#endif

#endif	/* HARDWARE_CONFIGURATION_H */

